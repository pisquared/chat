from flask import Flask, render_template, request, jsonify
from flask_login import current_user
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required

# Create app
app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'super-secret'
app.config['SECURITY_PASSWORD_SALT'] = 'super-secret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Create database connection object
db = SQLAlchemy(app)

# Define models
roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))


# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)


class ClientRegistry(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    address = db.Column(db.String)  # IP:port pair

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", )


class UserPair(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    left_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    left_user = db.relationship("User", foreign_keys=[left_user_id])

    right_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    right_user = db.relationship("User", foreign_keys=[right_user_id])


# Views
@app.route('/register_client', methods=['POST'])
@login_required
def register_client():
    address = request.form.get('address')
    client_registry = ClientRegistry.query.filter_by(user_id=current_user.id).first()
    if not client_registry:
        client_registry = ClientRegistry(user_id=current_user.id)
    client_registry.address = address
    db.session.add(client_registry)
    db.session.commit()
    return jsonify(dict(
        user_id=current_user.id,
        address=address,
    ))


@app.route('/user_pairs/search')
@login_required
def user_pair_search():
    email = request.args.get('email')
    user = User.query.filter_by(email=email).first()
    if user:
        return jsonify(dict(user_id=user.id))
    return jsonify(dict())


@app.route('/user_pairs/<int:right_user_id>/request')
@login_required
def user_pair_request(right_user_id):
    if current_user.id == right_user_id:
        return jsonify(dict(
            error="pair {}-{} can't be created: can't friend self".format(current_user.id, right_user_id)
        )), 400
    existing_pair = UserPair.query.filter_by(left_user_id=current_user.id, right_user_id=right_user_id).first()
    if existing_pair:
        accepted_pair = UserPair.query.filter_by(left_user_id=right_user_id, right_user_id=current_user.id).first()
        if accepted_pair:
            return jsonify(dict(
                error="pair {}-{} already accepted".format(current_user.id, right_user_id)
            )), 400
        else:
            return jsonify(dict(
                error="pair {}-{} already sent request".format(current_user.id, right_user_id)
            )), 400
    user_pair = UserPair(left_user_id=current_user.id,
                         right_user_id=right_user_id)
    db.session.add(user_pair)
    db.session.commit()
    return jsonify(), 201


@app.route('/user_pairs/<int:left_user_id>/accept')
@login_required
def user_pair_accept(left_user_id):
    existing_request = UserPair.query.filter_by(left_user_id=left_user_id, right_user_id=current_user.id).first()
    if not existing_request:
        return jsonify(dict(
            error="pair {}-{} does not exist".format(left_user_id, current_user.id)
        )), 400
    accepted_pair = UserPair.query.filter_by(left_user_id=current_user.id, right_user_id=left_user_id).first()
    if accepted_pair:
        return jsonify(dict(
            error="pair {}-{} already accepted".format(current_user.id, left_user_id)
        )), 400
    user_pair = UserPair(left_user_id=left_user_id,
                         right_user_id=current_user.id)
    db.session.add(user_pair)
    db.session.commit()
    return jsonify(), 201


@app.route('/user_pairs')
@login_required
def user_pairs():
    serialized_user_pairs = []
    user_pairs = UserPair.query.filter_by(left_user_id=current_user.id).all()
    for user_pair in user_pairs:
        other_pair = UserPair.query.filter_by(left_user_id=user_pair.right_user_id,
                                              right_user_id=current_user.id).first()
        if other_pair:
            status = "ACCEPTED"
        else:
            status = "REQUESTED"
        serialized_user_pairs.append(dict(user_id=user_pair.right_user_id,
                                          address=user_pair.right_user.address,
                                          status=status))
    return jsonify(serialized_user_pairs)


@app.route('/pair_requests')
@login_required
def pair_requests():
    pair_requests = UserPair.query.filter_by(right_user_id=current_user.id).all()
    serialized_pair_requests = [user_pair.left_user_id for user_pair in pair_requests]
    return jsonify(serialized_pair_requests)


if __name__ == '__main__':
    app.run(port=12345)
