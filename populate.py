from client_discovery.client_discovery import app as app1, db as db1, user_datastore as ud1
from client.client import app as app2, db as db2, user_datastore as ud2

app1.app_context().push()
db1.create_all()
ud1.create_user(email='user1@app.com', password='password')
ud1.create_user(email='user2@app.com', password='password')
db1.session.commit()

app2.app_context().push()
db2.create_all()
ud2.create_user(email='user1@app.com', password='password')
ud2.create_user(email='user2@app.com', password='password')
db2.session.commit()
